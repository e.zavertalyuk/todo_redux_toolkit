import React from 'react'
import Form from './components/Form'
import TodoItem from './components/TodoItem'
import User from './components/User'
import Posts from './components/Posts'
import { useSelector } from 'react-redux'
import Header from './components/Header'
import LoginForm from './components/LoginForm'

function App() {

    const { todos } = useSelector((state) => state.todo)
    const { isLogined } = useSelector((state) => state.isLogin)

    return (
        isLogined ?
            <>

                <Header />
                < div className='main-wrap' >
                    <div className='todo-wrap'>
                        <div className='todo todo-text'>
                            <h1 className='font-bold my-5'>Redux Toolkit State Change</h1>
                            <User />
                        </div>
                        <div className='todo todo-items'>
                            <h1 className='font-bold my-5'>Redux Toolkit Todo App</h1>
                            <Form />
                            {
                                todos.map((todo) => (
                                    <TodoItem
                                        key={todo.id}
                                        todo={todo}
                                    />
                                ))
                            }
                        </div>
                        <div className='todo todo-fetch'>
                            <h1 className='font-bold my-5'>Redux Toolkit Async Thunk</h1>
                            <Posts />
                        </div></div>
                </div ></>
            :
            <LoginForm />

    )
}

export default App
