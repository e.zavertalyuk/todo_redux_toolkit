import { createSlice } from '@reduxjs/toolkit';
let todos = localStorage.getItem('todos')
const initialState = {
   todos: JSON.parse(todos) || []
}
export const todoSlice = createSlice({
   name: 'todos',
   initialState,
   reducers: {
      addTodo: (state, action) => {
         state.todos.push(action.payload)
         localStorage.setItem('todos', JSON.stringify(state.todos))
      },
      removeTodo: (state, action) => {
         state.todos = state.todos.filter((todo) => todo.id !== action.payload)
         localStorage.setItem('todos', JSON.stringify(state.todos))
      },
      toogleTodo: (state, action) => {
         const toogleTodo = state.todos.find((todo) => todo.id === action.payload)
         toogleTodo.completed = !toogleTodo.completed
         localStorage.setItem('todos', JSON.stringify(state.todos))
      },
      removeAllTodos: (state, action) => {
         state.todos = []
         localStorage.removeItem('todos')
      }
   }
})

export const { addTodo, removeTodo, toogleTodo, removeAllTodos } = todoSlice.actions
export default todoSlice.reducer