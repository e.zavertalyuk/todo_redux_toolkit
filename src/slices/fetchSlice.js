import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
const initialState = {
   posts: [],
}
export const getPosts = createAsyncThunk(
   'posts/getPosts',
   async (_, { rejectWithValue, dispatch }) => {
      try {
         const response = await fetch('https://jsonplaceholder.typicode.com/posts')
         const data = await response.json()
         dispatch(setPosts(data))
         return data
      } catch (error) {
         return rejectWithValue(error.message)
      }
   }
)

export const deletePostById = createAsyncThunk('posts/deletePost',
   async (id, { rejectWithValue, dispatch }) => {
      await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
      dispatch(deletePost(id))
   },
)



export const postsSlice = createSlice({
   name: 'posts',
   initialState,
   reducers: {
      setPosts: (state, action) => {
         state.posts = action.payload
      },
      deletePost: (state, action) => {
         console.log(action.payload)
         state.posts = state.posts.filter(post => post.id !== action.payload)
      },
   },
   extraReducers: {
      [getPosts.fulfilled]: () => console.log('fullField'),
      [getPosts.pending]: () => console.log('pending'),
      [getPosts.rejected]: () => console.log('rejected'),
      [deletePostById.fulfilled]: () => console.log('Good'),

   }
})

export const { setPosts, deletePost } = postsSlice.actions
export default postsSlice.reducer