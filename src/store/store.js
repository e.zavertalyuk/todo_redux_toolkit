import { configureStore } from "@reduxjs/toolkit";

import userSlice from "../slices/userSlice";
import todoSlice from "../slices/todoSlice";
import postsSlice from "../slices/fetchSlice";
import loginSlice from "../slices/loginSlice";

export const store = configureStore({
   reducer: {
      user: userSlice,
      todo: todoSlice,
      fetchPosts: postsSlice,
      isLogin: loginSlice,
   }
})