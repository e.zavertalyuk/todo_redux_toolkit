import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { setFirstName, setLastName } from '../slices/userSlice'
const User = () => {
    const dispatch = useDispatch()
    const { firstName } = useSelector((state) => state.user)
    const { lastName } = useSelector((state) => state.user)

    return (
        <div className='flex flex-col'>
            <input
                type='text'
                placeholder='First Name'
                onChange={(e) => {
                    dispatch(setFirstName(e.target.value))
                }}
                className='w-full p-1 mb-2 focus:outline-none focus:border-lime-500 focus: border-2 placeholder:text-sm'
            />
            <input
                type='text'
                placeholder='Second Name'
                onChange={(e) => {
                    dispatch(setLastName(e.target.value))
                }}
                className='w-full p-1 mb-2 focus:outline-none focus:border-lime-500 focus: border-2 placeholder:text-sm'
            />
            <div className='flex gap-20 py-5'>
                <div className='flex flex-col'>
                    <div className='flex font-light'>First Name:</div>
                    <div className='flex'>
                        {firstName !== '' ? firstName : "No text"}
                    </div>
                </div>

                <div className='flex flex-col'>
                    <div className='flex font-light'>Last Name:</div>
                    <div className='flex'>
                        {lastName !== '' ? lastName : "No text"}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default User
