import React from 'react'
import { useDispatch } from 'react-redux'
import { removeTodo, toogleTodo } from '../slices/todoSlice'


const TodoItem = ({ todo }) => {
    const dispatch = useDispatch()

    const toogleTodoHandler = (id) => {
        dispatch(toogleTodo(id))
    }
    return (

        <div key={todo.id} className='todos-wrap'>
            <input
                onClick={() => toogleTodoHandler(todo.id)}
                type="checkbox"
                id={`step-${todo.id}`}
                className="input checkbox"
                checked={todo.completed}
                onChange={() => (console.log(1))}
            />

            <label htmlFor={`step-${todo.id}`} className={`todo-label ${todo.completed ? 'checked' : ''}`}>
                <p className={`todos-text ${todo.completed ? 'text-checked' : ''}`}>{todo.date} {todo.text}</p>
            </label>
            <div onClick={() => dispatch(removeTodo(todo.id))} className='text-sm px-2 py-2 flex bg-red-400 hover:bg-red-500 transition-all text-white cursor-pointer'>
                Delete
            </div>
        </div>
    )
}

export default TodoItem
